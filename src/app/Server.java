package app;


public class Server {

    // args: serverPort, clientPort
    public static void main(String[] args){
        try {
            int serverPort = Integer.parseInt(args[0]);
            int clientPort = Integer.parseInt(args[1]);
            ServerProtocol protocol = new ServerProtocol(serverPort, clientPort, args[2]);

            (new Thread(protocol)).start();
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
    }
}
