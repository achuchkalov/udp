package app;

import app.protocol.Protocol;
import app.protocol.SlidingWindow;
import app.protocol.WindowType;

public class ClientSlidingWindow extends SlidingWindow {

    public ClientSlidingWindow(){
        super();
        window = new WindowType(WINDOWSIZE, Protocol.DATASIZE);
    }

    public void putTicket(int tic){
        ticket.addLast(new Integer(tic));
    }

    public void put(byte [] x, int y){
        if(y >= expect) {
            window.addLast(x);
            ticket.addLast(y);
        }

    }

    public byte[] removePacket(){
        if(ticket.size() == 0){
            return null;
        }
        else {
            if (ticket.getFirst() == expect) {
                expect++;
                ticket.removeFirst();
                return window.removeFirst();
            } else
                return null;
        }
    }

    public void incExpect(){
        expect++;
    }
}
