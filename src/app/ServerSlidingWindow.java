package app;


import app.protocol.Protocol;
import app.protocol.SlidingWindow;
import app.protocol.WindowType;

public class ServerSlidingWindow extends SlidingWindow {

    public ServerSlidingWindow(){
        super();
        window = new WindowType(WINDOWSIZE, Protocol.DATASIZE + Integer.BYTES);
    }

    public void putTicket(int tic){
        synchronized (this) {
            if (tic == expect) {
                expect++;
                window.removeFirst();
                this.notifyAll();
                while ((ticket.size() > 0) && (ticket.getFirst() == expect)) {
                    window.removeFirst();
                    ticket.removeFirst();
                    expect++;
                }
            } else if (tic > expect)
                ticket.addLast(new Integer(tic));
        }
    }

    public void incExpect(){ return; }

    public byte[] removePacket() { return null; };

    public void put(byte [] x, int y) { return; }
}
