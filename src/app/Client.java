package app;

public class Client {

    // args: serverPort, clientPort
    public static void main(String[] args){
        try {
            int serverPort = Integer.parseInt(args[0]);
            int clientPort = Integer.parseInt(args[1]);
            ClientProtocol protocol = new ClientProtocol(clientPort, serverPort, args[2]);

            (new Thread(protocol)).start();
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
    }
}
