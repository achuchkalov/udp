package app;

import app.concurrent.Channel;
import app.protocol.Protocol;
import app.protocol.SenderCallBack;
import app.transport.Sender;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class ServerSender extends Sender {
    private SenderCallBack callBack;
    private FileInputStream fileInputStream = null;
    private byte[] data;
    private byte[] fileContents = new byte[Protocol.DATASIZE];
    private long numOfPacket;
    private Channel<byte[]> starter;

    public ServerSender(int notmyport, String filename, Channel<byte[]> starter, SenderCallBack callBack) {
        super(notmyport, null);
        this.callBack = callBack;
        this.starter = starter;

        try {
            fileInputStream = new FileInputStream(filename);
            long fileLength = new File(filename).length();
            numOfPacket = fileLength/ Protocol.DATASIZE + ((fileLength%Protocol.DATASIZE == 0)?0:1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void sendDatagram(byte[] data) throws InterruptedException, IOException {
        packet = new DatagramPacket(data, data.length, InetAddress.getLoopbackAddress(), notmyport);
        socket.send(packet);
    }

    public void loop() throws InterruptedException, IOException {
        data = callBack.tryToWait();
        if (data == null) {
            int len = fileInputStream.read(fileContents);

            if (len == -1) {
                fileInputStream.close();
                done = true;
                System.out.println("File reading ended.");
                return;
            }

            counter++;
            data = callBack.senderCallBack(fileContents, counter);
            System.out.println("Sending " + counter + " packet");
        }
        sendDatagram(data);
    }

    public void run() {
        try {
            // Ждем, пока главный поток не даст сигнал о начале работы
            starter.get();
            callBack.tryToWait();
            data = Protocol.longToByte(numOfPacket);
            counter++;
            data = callBack.senderCallBack(data, counter);
            sendDatagram(data);

            super.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-5);
        } catch (IOException e) {
           e.printStackTrace();
           System.exit(-5);
        }
    }
}