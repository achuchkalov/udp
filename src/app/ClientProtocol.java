package app;

import app.protocol.Protocol;
import app.concurrent.*;
import app.protocol.SlidingWindow;
import app.transport.*;

import java.io.*;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


public class ClientProtocol extends Protocol implements Runnable {

    private FileOutputStream fileOutputStream = null;
    private byte[] dataReceivedEarlier;

    public ClientProtocol(int clientPort, int serverPort, String outputFileName) {
        super(clientPort, serverPort);
        try {
            fileOutputStream = new FileOutputStream(outputFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-5);
        }
    }

    @Override
    protected SlidingWindow buildWindow() {
        return new ClientSlidingWindow();
    }

    @Override
    public void receiverCallBack(byte[] data) throws IOException, InterruptedException {
        int number = byteToInt(data, 0, 4);
        if(number == 0){
            System.out.println("Recieving number of packet");
            numOfpacket = byteToInt(data, 8, 4);
            window.incExpect();
            sndChannel.put(intToByte(number));
            return;
        }

        sndChannel.put(intToByte(number));
        System.out.println("Recieving " + number + " packet");
        if(window.isExpectedPacketClient(number)) {
            fileOutputStream.write(data, 4, data.length - 4);

            window.incExpect();
            while ((dataReceivedEarlier = window.removePacket()) != null) {
                fileOutputStream.write(dataReceivedEarlier, 4, dataReceivedEarlier.length - 4);
            }
            synchronized (window) {
                if (window.getExpect() > numOfpacket) {
                    stop();
                    socket.close();
                }
            }
        }
        else{
            window.put(data, number);
        }
    }


    @Override
    public void run() {
        try {
            try {
                socket = new DatagramSocket(ownedPort, InetAddress.getLoopbackAddress());
            } catch (SocketException e) {
                e.printStackTrace();
            }

            threadPool = new ThreadPool(2);
            threadPool.execute(new Receiver(this, socket));
            threadPool.execute(new ClientSender(foreignPort, sndChannel));
            sndChannel.put(intToByte(START_COMMAND));
        } catch (InterruptedException e){
            e.printStackTrace();
            System.exit(-5);
        }

    }
}
