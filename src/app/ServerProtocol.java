package app;

import app.concurrent.ThreadPool;
import app.protocol.Protocol;
import app.protocol.SlidingWindow;
import app.transport.Receiver;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


public class ServerProtocol extends Protocol implements Runnable {

    private String inputFileName;
    private static byte[] START_ANSWER = new byte[0];

    public ServerProtocol(int serverPort, int clientPort, String _inputFileName) {
        super(serverPort, clientPort);
        inputFileName = _inputFileName;
    }

    @Override
    protected SlidingWindow buildWindow() {
        return new ServerSlidingWindow();
    }

    @Override
    public void receiverCallBack(byte[] data) throws IOException, InterruptedException {
        int number = byteToInt(data, 0, 4);
        if(number == START_COMMAND){
            sndChannel.put(START_ANSWER);
            return;
        }
        System.out.println("Recieving " + number + " ticket");
        window.putTicket(number);
        if(window.getExpect() > numOfpacket) {
            stop();
            socket.close();
        }
    }


    @Override
    public void run() {
        try {
            System.out.println("Waiting for client to connect to send " + inputFileName);
            try {
                socket = new DatagramSocket(ownedPort, InetAddress.getLoopbackAddress());
            } catch (SocketException e) {
                e.printStackTrace();
            }

            threadPool = new ThreadPool(2);
            threadPool.execute(new Receiver(this, socket));
            threadPool.execute(new ServerSender(foreignPort, inputFileName, sndChannel, this));
        } catch (InterruptedException e){
            e.printStackTrace();
            System.exit(-5);
        }

    }
}
