package app.transport;

import app.concurrent.Channel;
import app.protocol.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;


public abstract class Sender implements Runnable {
    protected int counter;

    protected Channel<byte[]> channel;
    protected SenderCallBack callBack;
    protected DatagramSocket socket = null;
    protected int notmyport;
    protected boolean server;
    protected byte[] data;
    protected DatagramPacket packet;
    protected boolean done = false;

    public Sender(int notmyport, Channel<byte[]> channel) {
        this.channel = channel;
        this.notmyport = notmyport;
        this.counter = -1;
        this.server = !(callBack == null);
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public abstract void loop() throws InterruptedException, IOException;

    public void run() {
        //первый пакет, который посылается, это пакет с количеством пакетов, чтобы было понятно, когда заканчивать принимать клиенту
        // номер этого пакета 0
        try {
            data = null;
            while (!done) {
                loop();
            }
        } catch (InterruptedException e){
            System.out.println("Sender: done");
            socket.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
