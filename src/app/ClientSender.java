package app;

import app.concurrent.Channel;
import app.protocol.Protocol;
import app.transport.Sender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class ClientSender extends Sender {
    public ClientSender(int notmyport, Channel<byte[]> channel) {
        super(notmyport, channel);
    }

    public void loop() throws InterruptedException, IOException {
        data = channel.get();
        System.out.println("Sending " + Protocol.byteToInt(data,0,4) + " ticket");
        packet = new DatagramPacket(data, data.length, InetAddress.getLoopbackAddress(), notmyport);
        socket.send(packet);
    }
}