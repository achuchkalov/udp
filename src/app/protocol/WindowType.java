package app.protocol;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class WindowType {
    private final int size;
    private final int sizeByte;
    private byte [][] array;
    private int currentsize;
    private int first;
    private int last;

    private Object lock = new Object();

    public WindowType(int size, int sizeByte){
        this.size = size;
        this.sizeByte = sizeByte;
        array = new byte[size][];
        for (int i = 0; i < size; i++){
            array[i] = new byte[sizeByte];
        }
        first = 0;
        last = -1;
        currentsize = 0;
    }
    public boolean isFull(){
        return currentsize == size;
    }
    public boolean isEmpty(){
        return currentsize == 0;
    }
    public void addLast(byte[] x){
        synchronized (lock) {
            if (!isFull()) {
                if (isEmpty())
                    last = first;
                else {
                    if (last == size - 1)
                        last = 0;
                    else
                        last++;
                }
                currentsize++;
                System.arraycopy(x, 0, array[last], 0, x.length);
                if(x.length != sizeByte){
                    Arrays.fill(array[last], x.length, sizeByte - 1, (byte) 0 );
                }
            } else throw new ArrayIndexOutOfBoundsException();
        }
    }
    public byte[] getFirst(){
        synchronized (lock) {
            if (!isEmpty()) {
                byte[] byt = new byte[sizeByte];
                System.arraycopy(array[first], 0, byt, 0, sizeByte);
                return byt;
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    public byte[] removeFirst(){
        synchronized (lock) {
            if (!isEmpty()) {
                int temp = first;
                if (first == 0)
                    first = size - 1;
                else
                    first--;
                currentsize--;
                byte[] byt = new byte[sizeByte];
                System.arraycopy(array[temp], 0, byt, 0, sizeByte);
                return byt;
            } else {
                throw new NoSuchElementException();
            }
        }
    }
    public int size(){
        return currentsize;
    }
}
