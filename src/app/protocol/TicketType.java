package app.protocol;

import java.util.NoSuchElementException;

import static app.protocol.SlidingWindow.*;

public class TicketType {
    private final int size;
    private int [] array;
    private int currentsize;
    private int first;
    private int last;

    private Object lock = new Object();

    public TicketType(int size){
        this.size = size;
        array = new int[size];
        first = 0;
        last = -1;
        currentsize = 0;
    }
    public boolean isFull(){
        return currentsize == size;
    }
    public boolean isEmpty(){
        return currentsize == 0;
    }
    public void addLast(int x){
        synchronized (lock) {
            if (!isFull()) {
                if (isEmpty())
                    last = first;
                else {
                    if (last == size - 1)
                        last = 0;
                    else
                        last++;
                }
                currentsize++;
                array[last] = x;
            } else
                throw new ArrayIndexOutOfBoundsException();
        }
    }
    public int getFirst(){
        synchronized (lock) {
            if (!isEmpty()) {
                return array[first];
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    public int removeFirst(){
        synchronized (lock) {
            if (!isEmpty()) {
                int temp = first;
                if (first == 0)
                    first = size - 1;
                else
                    first--;
                currentsize--;
                return array[temp];
            } else {
                throw new NoSuchElementException();
            }
        }
    }

    public int size() {
        return currentsize;
    }
}
