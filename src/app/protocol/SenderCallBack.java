package app.protocol;


public interface SenderCallBack {
    public byte[] tryToWait() throws InterruptedException;
    public byte[] senderCallBack(byte[] buf, int counter) throws InterruptedException;
}
