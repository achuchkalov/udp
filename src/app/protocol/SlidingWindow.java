package app.protocol;


public abstract class SlidingWindow {
    public final static int WINDOWSIZE = 5;

    protected int expect;

    protected WindowType window;
    protected TicketType ticket;

    public SlidingWindow(){
        expect = 0;
        ticket = new TicketType(WINDOWSIZE);
    }

    public boolean isFull(){
        return (window.size()) >= WINDOWSIZE;
    }

    public boolean isExpectedPacketClient(int count){
        return count == expect;
    }

    public int getExpect(){
        return expect;
    }

    public void putPacket(byte[] ob){
        synchronized (this) {
            if (!isFull()) {
                window.addLast(ob);
            }
        }
    }

    public byte[] getPacket(){
        return window.getFirst();
    }

    public abstract void putTicket(int tic);
    public abstract void incExpect();
    public abstract void put(byte [] x, int y);
    public abstract byte[] removePacket();
}
