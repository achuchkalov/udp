package app.protocol;

import app.concurrent.Channel;
import app.concurrent.ThreadPool;

import java.io.*;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;


public abstract class Protocol implements ReceiverCallBack, SenderCallBack {

    public final static int DATASIZE = 1000;
    public final static int SNDCHANNELSIZE = 10;
    public final static int DATACHANNELSIZE = 10;
    public final static int TIMEOUT = 3000;
    protected final static int START_COMMAND = -1;

    protected Channel<byte[]> sndChannel;
    protected Channel<byte[]> dataChannel;

    protected ThreadPool threadPool;

    // Порт, на котором работает Receiver. Для сервера и клиента это разные порты.
    protected int ownedPort;
    // Порт, куда отправляет Sender. Для сервера и клиента это разные порты.
    protected int foreignPort;

    protected long numOfpacket;
    protected SlidingWindow window;

    protected DatagramSocket socket;

    public Protocol(int ownedPort, int foreignPort){
        sndChannel = new Channel<byte[]>(SNDCHANNELSIZE);
        dataChannel = new Channel<byte[]>(DATACHANNELSIZE);
        window = buildWindow();
        this.ownedPort = ownedPort;
        this.foreignPort = foreignPort;
    }

    static public byte[] intToByte(int data) {
        return ByteBuffer.allocate(4).putInt(data).array();
    }
    static public int byteToInt(byte[] data, int offset, int length) {
        return ByteBuffer.wrap(data, offset, length).getInt();
    }
    static public byte[] longToByte(long data) {
        return ByteBuffer.allocate(8).putLong(data).array();
    }
    static public long byteToLong(byte[] data, int offset, int length) {
        return ByteBuffer.wrap(data, offset, length).getLong();
    }
    @Override
    public abstract void receiverCallBack(byte[] data) throws IOException, InterruptedException;

    protected abstract SlidingWindow buildWindow();

    @Override
    public byte[] tryToWait() throws InterruptedException {
        synchronized (window) {
            if (window.isFull()) {
                window.wait(TIMEOUT);
                if(window.isFull()) {
                    System.out.println("resend");
                    return window.getPacket();
                }
                else
                    return null;
            }
            else
                return null;

        }
    }

    @Override
    public byte[] senderCallBack(byte[] buf, int counter) throws InterruptedException {
        if(counter == 0){
            numOfpacket = byteToLong(buf, 0, 8);
        }
        byte[] data = new byte[buf.length + 4];
        System.arraycopy(intToByte(counter), 0, data, 0, 4);
        System.arraycopy(buf, 0, data, 4, buf.length);
        window.putPacket(data);

        return data;
    }


    public void stop(){
        threadPool.stop();
    }
}
